<?php
namespace Drupal\tfa_basic;

/**
 * Class TfaBasicHelp
 */
class TfaBasicHelp extends TfaBasePlugin implements TfaValidationPluginInterface {

  /**
   * @copydoc TfaBasePlugin::getForm()
   */
  public function getForm(array $form, array &$form_state) {
    $default = t('Contact support to reset your access');
    // @FIXME
// Could not extract the default value because it is either indeterminate, or
// not scalar. You'll need to provide a default value in
// config/install/tfa_basic.settings.yml and config/schema/tfa_basic.schema.yml.
$content = \Drupal::config('tfa_basic.settings')->get('tfa_basic_help_text');
    $form['help'] = array(
      '#type' => 'markup',
      '#markup' => \Drupal\Component\Utility\Html::escape($content),
    );
    // Disallow login plugins from applying to this step.
    $form['#tfa_no_login'] = TRUE;
    return $form;
  }

  /**
   * @copydoc TfaValidationPluginInterface::validateForm()
   */
  public function validateForm(array $form, array &$form_state) {
    // Unused.
  }

}
