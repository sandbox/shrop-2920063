

<!-- THEME DEBUG -->
<!-- THEME HOOK: 'dmu_form' -->
<!-- BEGIN OUTPUT from 'modules/contrib/drupalmoduleupgrader/templates/Form.html.twig' -->
<?php

/**
 * @file
 * Contains \Drupal\tfa_basic\Form\TfaBasicOverview.
 */

namespace Drupal\tfa_basic\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

class TfaBasicOverview extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tfa_basic_overview';
  }

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state, $account = NULL) {

    $output['info'] = [
      '#type' => 'markup',
      '#markup' => '<p>' . t('Two-factor authentication (TFA) provides additional security for your account. With TFA enabled, you log in to the site with a verification code in addition to your username and password.') . '</p>',
    ];
    $form_state->setStorage($account);
    $user_tfa = tfa_basic_get_tfa_data($account);
    $enabled = isset($user_tfa['status']) && $user_tfa['status'] ? TRUE : FALSE;

    if (!empty($user_tfa)) {
      if ($enabled) {
        // @FIXME
// url() expects a route name or an external URI.
// $status_text = t('Status: <strong>TFA enabled</strong>, set !time. <a href="!url">Disable TFA</a>', array('!time' => format_date($user_tfa['saved']), '!url' => url('user/' . $account->uid . '/security/tfa/disable')));

      }
      else {
        $status_text = t('Status: <strong>TFA disabled</strong>, set !time.', [
          '!time' => format_date($user_tfa['saved'])
          ]);
      }
      $output['status'] = [
        '#type' => 'markup',
        '#markup' => '<p>' . $status_text . '</p>',
      ];
    }

    // Start with validate plugin setup.
    if (!$enabled) {
      // @FIXME
// // @FIXME
// // This looks like another module's variable. You'll need to rewrite this call
// // to ensure that it uses the correct configuration object.
// $validate_plugin = variable_get('tfa_validate_plugin', '');

      $output['setup'] = _tfa_basic_plugin_setup_form_overview($validate_plugin, $account, $user_tfa);
    }
    else {
      // TOTP setup.
      $output['app'] = _tfa_basic_plugin_setup_form_overview('tfa_basic_totp', $account, $user_tfa);
      // SMS setup.
      $output['sms'] = _tfa_basic_plugin_setup_form_overview('tfa_basic_sms', $account, $user_tfa);
      // Trusted browsers.
      $output['trust'] = _tfa_basic_plugin_setup_form_overview('tfa_basic_trusted_browser', $account, $user_tfa);
      // Recovery codes.
      $output['recovery'] = _tfa_basic_plugin_setup_form_overview('tfa_basic_recovery_code', $account, $user_tfa);
    }

    return $output;
  }

}
?>

<!-- END OUTPUT from 'modules/contrib/drupalmoduleupgrader/templates/Form.html.twig' -->

