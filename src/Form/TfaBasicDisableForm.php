

<!-- THEME DEBUG -->
<!-- THEME HOOK: 'dmu_form' -->
<!-- BEGIN OUTPUT from 'modules/contrib/drupalmoduleupgrader/templates/Form.html.twig' -->
<?php

/**
 * @file
 * Contains \Drupal\tfa_basic\Form\TfaBasicDisableForm.
 */

namespace Drupal\tfa_basic\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

class TfaBasicDisableForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tfa_basic_disable_form';
  }

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state, $account = NULL) {
    $user = \Drupal::currentUser();

    $form_state->setStorage($account);

    if ($account->uid != $user->uid && \Drupal::currentUser()->hasPermission('administer users')) {
      $preamble_desc = t('Are you sure you want to disable TFA on account %name?', [
        '%name' => $account->name
        ]);
      $notice_desc = t('TFA settings and data will be lost. %name can re-enable TFA again from their profile.', [
        '%name' => $account->name
        ]);
      if (tfa_basic_tfa_required($account)) {
        drupal_set_message(t("This account is required to have TFA enabled per the 'require TFA' permission on one of their roles. Disabling TFA will remove their ability to log back into the site. If you continue, consider also removing the role so they can authenticate and setup TFA again."), 'warning');
      }
    }
    else {
      $preamble_desc = t('Are you sure you want to disable your two-factor authentication setup?');
      $notice_desc = t("Your settings and data will be lost. You can re-enable two-factor authentication again from your profile.");
      if (tfa_basic_tfa_required($account)) {
        drupal_set_message(t('Your account must have at least one two-factor authentication method enabled. Continuing will disable your ability to log back into this site.'), 'warning');
        $notice_desc = t('Your settings and data will be lost and you will be unable to log back into the site. To regain access contact a site administrator.');
      }
    }
    $form['preamble'] = [
      '#prefix' => '<p class="preamble">',
      '#suffix' => '</p>',
      '#markup' => $preamble_desc,
    ];
    $form['notice'] = [
      '#prefix' => '<p class="preamble">',
      '#suffix' => '</p>',
      '#markup' => $notice_desc,
    ];

    $form['account']['current_pass'] = [
      '#type' => 'password',
      '#title' => t('Confirm your current password'),
      '#description_display' => 'before',
      '#size' => 25,
      '#weight' => -5,
      '#attributes' => [
        'autocomplete' => 'off'
        ],
      '#required' => TRUE,
    ];
    $form['account']['mail'] = [
      '#type' => 'value',
      '#value' => $account->mail,
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Disable'),
    ];
    $form['actions']['cancel'] = [
      '#type' => 'submit',
      '#value' => t('Cancel'),
      '#limit_validation_errors' => [],
      '#submit' => [
        'tfa_basic_disable_form_submit'
        ],
    ];

    return $form;
  }

  public function validateForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $user = \Drupal::currentUser();
    $account = $form_state->getStorage();
    // Allow administrators to disable TFA for another account.
    if ($account->uid != $user->uid && \Drupal::currentUser()->hasPermission('administer users')) {
      $account = $user;
    }
    // Check password. (from user.module user_validate_current_pass()).
    // @FIXME
    // // @FIXME
    // // This looks like another module's variable. You'll need to rewrite this call
    // // to ensure that it uses the correct configuration object.
    // require_once \Drupal::root() . '/' . variable_get('password_inc', 'includes/password.inc');

    $current_pass = user_check_password($form_state->getValue(['current_pass']), $account);
    if (!$current_pass) {
      $form_state->setErrorByName('current_pass', t("Incorrect password."));
    }
  }

  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $account = $form_state->getStorage();
    if ($form_state->getValue(['op']) === $form_state->getValue(['cancel'])) {
      drupal_set_message(t('TFA disable canceled.'));
      $form_state->set(['redirect'], 'user/' . $account->uid . '/security/tfa');
      return;
    }
    $params = ['account' => $account];
    tfa_basic_setup_save_data($account, ['status' => FALSE]);
    // Delete TOTP code.
    $totp = new TfaTotp(['uid' => $account->uid]);
    $totp->deleteSeed();
    // Delete recovery codes.
    $recovery = new TfaBasicRecoveryCodeSetup([
      'uid' => $account->uid
      ]);
    $recovery->deleteCodes();
    // Delete trusted browsers.
    $trusted = new TfaTrustedBrowserSetup([
      'uid' => $account->uid
      ]);
    $trusted->deleteTrustedBrowsers();

    \Drupal::logger('tfa_basic')->notice('TFA disabled for user @name UID !uid', [
      '@name' => $account->name,
      '!uid' => $account->uid,
    ]);

    // E-mail account to inform user that it has been disabled.
    drupal_mail('tfa_basic', 'tfa_basic_disabled_configuration', $account->mail, user_preferred_language($account), $params);

    drupal_set_message(t('TFA has been disabled.'));
    $form_state->set(['redirect'], 'user/' . $account->uid . '/security/tfa');
  }

}
?>

<!-- END OUTPUT from 'modules/contrib/drupalmoduleupgrader/templates/Form.html.twig' -->

