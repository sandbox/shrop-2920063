

<!-- THEME DEBUG -->
<!-- THEME HOOK: 'dmu_form' -->
<!-- BEGIN OUTPUT from 'modules/contrib/drupalmoduleupgrader/templates/Form.html.twig' -->
<?php

/**
 * @file
 * Contains \Drupal\tfa_basic\Form\TfaBasicSetupForm.
 */

namespace Drupal\tfa_basic\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

class TfaBasicSetupForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tfa_basic_setup_form';
  }

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state, $account = NULL, $method = NULL) {
    $user = \Drupal::currentUser();

    $form['account'] = [
      '#type' => 'value',
      '#value' => $account,
    ];
    $tfa_data = tfa_basic_get_tfa_data($account);
    $enabled = isset($tfa_data['status']) && $tfa_data['status'] ? TRUE : FALSE;

    // Always require a password on the first time through.
    if (!$form_state->getStorage()) {
      // Allow administrators to change TFA settings for another account.
      if ($account->uid != $user->uid && \Drupal::currentUser()->hasPermission('administer users')) {
        $current_pass_description = t('Enter your current password to alter TFA settings for account %name.', [
          '%name' => $account->name
          ]);
      }
      else {
        $current_pass_description = t('Enter your current password to continue.');
      }
      $form['current_pass'] = [
        '#type' => 'password',
        '#title' => t('Current password'),
        '#size' => 25,
        '#required' => TRUE,
        '#description' => $current_pass_description,
        '#attributes' => [
          'autocomplete' => 'off'
          ],
      ];
      $form['submit'] = [
        '#type' => 'submit',
        '#value' => t('Confirm'),
      ];
      $form['cancel'] = [
        '#type' => 'submit',
        '#value' => t('Cancel'),
        '#limit_validation_errors' => [],
        '#submit' => [
          'tfa_basic_setup_form_submit'
          ],
      ];
    }
    else {
      // If TFA is not enabled setup each plugin by using enabled plugins as form
    // steps.
      if (!$enabled && !$form_state->getStorage()) {
        $form_state->setStorage(TRUE);
        $steps = _tfa_basic_full_setup_steps($method);
        $form_state->setStorage($steps);
        $form_state->setStorage([]);
      }

      // Override provided method if operating under multi-step.
      if (!$form_state->getStorage()) {
        $method = $form_state->getStorage();
      }
      // Record methods progressed.
      $form_state->setStorage($method);
      $context = ['uid' => $account->uid];
      switch ($method) {
        case 'tfa_basic_totp':
          // @FIXME
          // drupal_set_title() has been removed. There are now a few ways to set the title
          // dynamically, depending on the situation.
          // 
          // 
          // @see https://www.drupal.org/node/2067859
          // drupal_set_title(t('TFA setup - Application'));

          $setup_plugin = new TfaTotpSetup($context);
          $tfa_setup = new TfaSetup($setup_plugin, $context);

          if (!empty($tfa_data)) {
            $form['disclaimer'] = [
              '#type' => 'markup',
              '#markup' => '<p>' . t('Note: You should delete the old account in your mobile or desktop app before adding this new one.') . '</p>',
            ];
          }
          $form = $tfa_setup->getForm($form, $form_state);
          $form_state->setStorage($tfa_setup);
          break;

        case 'tfa_basic_trusted_browser':
          // @FIXME
          // drupal_set_title() has been removed. There are now a few ways to set the title
          // dynamically, depending on the situation.
          // 
          // 
          // @see https://www.drupal.org/node/2067859
          // drupal_set_title(t('TFA setup - Trusted browsers'));

          $setup_plugin = new TfaTrustedBrowserSetup($context);
          $tfa_setup = new TfaSetup($setup_plugin, $context);
          $form = $tfa_setup->getForm($form, $form_state);
          $form_state->setStorage($tfa_setup);
          break;

        case 'tfa_basic_recovery_code':
          // @FIXME
          // drupal_set_title() has been removed. There are now a few ways to set the title
          // dynamically, depending on the situation.
          // 
          // 
          // @see https://www.drupal.org/node/2067859
          // drupal_set_title(t('TFA setup - Recovery codes'));

          $setup_plugin = new TfaBasicRecoveryCodeSetup($context);
          $tfa_setup = new TfaSetup($setup_plugin, $context);
          $form = $tfa_setup->getForm($form, $form_state);
          $form_state->setStorage($tfa_setup);
          break;

        case 'tfa_basic_sms':
          // @FIXME
          // drupal_set_title() has been removed. There are now a few ways to set the title
          // dynamically, depending on the situation.
          // 
          // 
          // @see https://www.drupal.org/node/2067859
          // drupal_set_title(t('TFA setup - SMS'));

          // SMS itself has multiple steps. Begin with phone number entry.
          if (!$form_state->getStorage()) {
            $default_number = tfa_basic_get_mobile_number($account);
            $form['sms_number'] = [
              '#type' => 'textfield',
              '#title' => t('Mobile phone number'),
              '#required' => TRUE,
              '#description' => t('Enter your mobile phone number that can receive SMS codes. A code will be sent to this number for validation.'),
              '#default_value' => $default_number ? : '',
            ];
            $phone_field = \Drupal::config('tfa_basic.settings')->get('tfa_basic_phone_field');
            if (!empty($phone_field)) {
              // Report that this is an account field.
              $field = field_info_instance('user', $phone_field, 'user');
              $form['sms_number']['#description'] .= ' ' . t('This number is stored on your account under field %label.', [
                '%label' => $field['label']
                ]);
            }
            $form['send'] = [
              '#type' => 'submit',
              '#value' => t('Send SMS'),
            ];
            if (!empty($tfa_data['data']['sms'])) {
              // Provide disable SMS option.
              $form['actions']['sms_disable'] = [
                '#type' => 'submit',
                '#value' => t('Disable SMS delivery'),
                '#limit_validation_errors' => [],
                '#submit' => [
                  'tfa_basic_setup_form_submit'
                  ],
              ];
            }
          }
            // Then validate by sending an SMS.
          else {
            $number = tfa_basic_format_number($form_state->getStorage());
            drupal_set_message(t("A code was sent to @number. It may take up to a minute for its arrival.", [
              '@number' => $number
              ]));
            $tfa_setup = $form_state->getStorage();
            $form = $tfa_setup->getForm($form, $form_state);
            if (!$form_state->getStorage()) {
              drupal_set_message(t("If the code does not arrive or you entered the wrong number skip this step to continue without SMS delivery. You can enable it after completing the rest of TFA setup."));
            }
            else {
              // @FIXME
// l() expects a Url object, created from a route name or external URI.
// $form['sms_code']['#description'] .= ' ' . l(t('If the code does not arrive or you entered the wrong number click here to start over.'), 'user/' . $account->uid . '/security/tfa/sms-setup');

            }

            $form_state->setStorage($tfa_setup);
          }
          break;

        // List previously saved recovery codes. Note, this is not a plugin.
        case 'recovery_codes_list':
          $recovery = new TfaBasicRecoveryCodeSetup(['uid' => $account->uid]);
          $codes = $recovery->getCodes();

          // @FIXME
          // theme() has been renamed to _theme() and should NEVER be called directly.
          // Calling _theme() directly can alter the expected output and potentially
          // introduce security issues (see https://www.drupal.org/node/2195739). You
          // should use renderable arrays instead.
          // 
          // 
          // @see https://www.drupal.org/node/2195739
          // $output = theme('item_list', array('items' => $codes));

          // @FIXME
          // l() expects a Url object, created from a route name or external URI.
          // $output .= l(t('Return to account TFA overview'), 'user/' . $account->uid . '/security/tfa');

          $form['output'] = [
            '#type' => 'markup',
            '#markup' => $output,
          ];
          // Return early.
          return $form;

        default:
          break;
      }
      // Provide skip button under full setup.
      if (!$form_state->getStorage() && count($form_state->getStorage()) > 1) {
        $count = count($form_state->getStorage());
        $form['actions']['skip'] = [
          '#type' => 'submit',
          '#value' => $count > 0 ? t('Skip') : t('Skip and finish'),
          '#limit_validation_errors' => [],
          '#submit' => [
            'tfa_basic_setup_form_submit'
            ],
        ];
      }
        // Provide cancel button on first step or single steps.
      else {
        $form['actions']['cancel'] = [
          '#type' => 'submit',
          '#value' => t('Cancel'),
          '#limit_validation_errors' => [],
          '#submit' => [
            'tfa_basic_setup_form_submit'
            ],
        ];
      }
      // Record the method in progress regardless of whether in full setup.
      $form_state->setStorage($method);
    }

    return $form;

  }

  public function validateForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $user = \Drupal::currentUser();
    $account = $form['account']['#value'];
    if (!$form_state->getValue(['current_pass'])) {
      // Allow administrators to change TFA settings for another account.
      if ($account->uid != $user->uid && \Drupal::currentUser()->hasPermission('administer users')) {
        $account = $user;
      }
      // Check password. (from user.module user_validate_current_pass()).
      // @FIXME
      // // @FIXME
      // // This looks like another module's variable. You'll need to rewrite this call
      // // to ensure that it uses the correct configuration object.
      // require_once \Drupal::root() . '/' . variable_get('password_inc', 'includes/password.inc');

      $current_pass = user_check_password($form_state->getValue(['current_pass']), $account);
      if (!$current_pass) {
        $form_state->setErrorByName('current_pass', t("Incorrect password."));
      }
      return;
    }
    elseif (!$form_state->getValue(['cancel']) && $form_state->getValue(['op']) === $form_state->getValue([
      'cancel'
      ])) {
      return;
    }
      // Handle first step of SMS setup.
    elseif (!$form_state->getValue([
      'sms_number'
      ])) {
      // Validate number.
      $number = $form_state->getValue(['sms_number']);
      $number_errors = tfa_basic_valid_number($number);
      if (!empty($number_errors)) {
        foreach ($number_errors as $error) {
          $form_state->setErrorByName('number', $error);
        }
      }
      return;
    }
      // Validate plugin form.
    elseif ($form_state->getStorage()) {
      $method = $form_state->getStorage();
      $tfa_setup = $form_state->getStorage();
      if (!$tfa_setup->validateForm($form, $form_state)) {
        foreach ($tfa_setup->getErrorMessages() as $element => $message) {
          $form_state->setErrorByName($element, $message);
        }
      }
    }
  }

  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $account = $form['account']['#value'];

    // Cancel button.
    if (!$form_state->getValue(['cancel']) && $form_state->getValue(['op']) === $form_state->getValue(['cancel'])) {
      drupal_set_message('TFA setup canceled.');
      $form_state->set(['redirect'], 'user/' . $account->uid . '/security/tfa');
      return;
    }
    // Password validation.
    if (!$form_state->getValue(['current_pass'])) {
      $form_state->setStorage(TRUE);
      $form_state->setRebuild(TRUE);
      return;
    }
      // Submitting mobile number step.
    elseif ($form_state->getValue(['sms_number'])) {
      // Send code to number.
      $form_state->setStorage($form_state->getValue(['sms_number']));
      $context = [
        'uid' => $account->uid,
        'mobile_number' => $form_state->getStorage(),
      ];
      $client = tfa_basic_get_twilio_client();
      $setup_plugin = new TfaBasicSmsSetup($context, $form_state->getStorage(), $client);
      $tfa_setup = new TfaSetup($setup_plugin, $context);
      $tfa_setup->begin();
      $errors = $tfa_setup->getErrorMessages();
      if (!empty($errors)) {
        foreach ($errors as $error) {
          $form_state->setErrorByName('number', $error);
        }
      }
      else {
        // No errors so store setup.
        $form_state->setStorage($tfa_setup);
      }
      $form_state->setRebuild(TRUE);
      return;
    }
    // Disabling SMS delivery.
    if (!$form_state->getValue(['sms_disable']) && $form_state->getValue(['op']) === $form_state->getValue(['sms_disable'])) {
      tfa_basic_setup_save_data($account, ['sms' => FALSE]);
      drupal_set_message(t('TFA SMS delivery disabled.'));
      $form_state->set(['redirect'], 'user/' . $account->uid . '/security/tfa');
      \Drupal::logger('tfa_basic')->info('TFA SMS disabled for user @name UID !uid', [
        '@name' => $account->name,
        '!uid' => $account->uid,
      ]);
      return;
    }
      // Submitting a plugin form.
    elseif ($form_state->getStorage()) {
      $method = $form_state->getStorage();
      $skipped_method = FALSE;

      // Support skipping optional steps when in full setup.
      if (!$form_state->getValue(['skip']) && $form_state->getValue(['op']) === $form_state->getValue(['skip'])) {
        $skipped_method = $method;
        $form_state->setStorage($method);
        unset($form_state->getStorage());
      }

      // Trigger multi-step if in full setup.
      if ($form_state->getStorage()) {
        _tfa_basic_set_next_step($form_state, $method, $skipped_method);
      }

      // Plugin form submit.
      if ($form_state->getStorage()) {
        $setup_class = $form_state->getStorage();
        if (!$setup_class->submitForm($form, $form_state)) {
          drupal_set_message(t('There was an error during TFA setup. Your settings have not been saved.'), 'error');
          $form_state->set(['redirect'], 'user/' . $account->uid . '/security/tfa');
          return;
        }
      }

      // Save user TFA settings for relevant plugins that weren't skipped.
      if (empty($skipped_method) && $method == 'tfa_basic_sms' && !$form_state->getStorage() && in_array('tfa_basic_sms', $form_state->getStorage())) {

        // Update mobile number if different than stored.
        if ($form_state->getStorage() !== tfa_basic_get_mobile_number($account)) {
          tfa_basic_set_mobile_number($account, $form_state->getStorage());
        }
        tfa_basic_setup_save_data($account, ['sms' => TRUE]);
      }

      // Return if multi-step.
      if (!$form_state->isRebuilding() && $form_state->isRebuilding()) {
        return;
      }
      // Else, setup complete and return to overview page.
      drupal_set_message(t('TFA setup complete.'));
      $form_state->set(['redirect'], 'user/' . $account->uid . '/security/tfa');

      // Log and notify if this was full setup.
      if ($form_state->getStorage()) {
        $data = [
          'plugins' => array_diff($form_state->getStorage(), $form_state->getStorage())
          ];
        tfa_basic_setup_save_data($account, $data);
        $params = ['account' => $account];
        drupal_mail('tfa_basic', 'tfa_basic_tfa_enabled', $account->mail, user_preferred_language($account), $params);
        \Drupal::logger('tfa_basic')->info('TFA enabled for user @name UID !uid', [
          '@name' => $account->name,
          '!uid' => $account->uid,
        ]);
      }
    }
  }

}
?>

<!-- END OUTPUT from 'modules/contrib/drupalmoduleupgrader/templates/Form.html.twig' -->

